from django.shortcuts import render
from lab_2.models import Note
from django.http.response import HttpResponse
from django.core import serializers

# Create your views here.
def index(request):
	notes = Note.objects.all()
	response = {'notes':notes}
	return render(request, 'lab5_index.html')

def get_note(request, id_note):
	data = serializers.serialize('json',Note.objects.filter(id=id_note))
	return HttpResponse(data, content_type="application/json")