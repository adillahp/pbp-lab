from django.urls import path
from .views import index, get_note

urlpatterns = [
    path('', index, name='index'),
    path('notes/<id_note>', get_note),
]
