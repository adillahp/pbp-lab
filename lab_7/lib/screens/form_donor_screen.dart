import 'package:flutter/material.dart';
import '../widgets/form_donor.dart';

class FormDonorScreen extends StatefulWidget {
  final RichText title;

  FormDonorScreen(this.title);
  static const routeName = '/form_donor';

  @override
  _FormDonorScreenState createState() => _FormDonorScreenState();
}

class _FormDonorScreenState extends State<FormDonorScreen> {
	@override
	Widget build(BuildContext context) {
    return Scaffold(
		appBar: AppBar(
			title: widget.title,
			iconTheme: IconThemeData(color: Colors.red),
			backgroundColor: Color(0xfff8f9fa),
		),
		backgroundColor: Color(0xffdceaf9),
		body: SingleChildScrollView(child:Stack(
			children: <Widget>[
				Column(
					crossAxisAlignment: CrossAxisAlignment.start,
					mainAxisAlignment: MainAxisAlignment.start,
					children: <Widget>[
						Container(
							color: Colors.white,
							//margin:EdgeInsets.only(top:80, left:20, right:20),
							child: FormDonor(),
						),
				    ],
				),
			],
		),),
	);
	}
}