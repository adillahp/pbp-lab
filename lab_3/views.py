from django.shortcuts import render, redirect
from .forms import FriendForm
from lab_1.models import Friend
from django.contrib.auth.decorators import login_required

@login_required(login_url='/admin/login/')
def index(request):
	friends = Friend.objects.all()
	response = {'friends': friends}
	return render(request, 'lab3_index.html', response)

@login_required(login_url='/admin/login/')
def add_friend(request):
	form = FriendForm(request.POST or None)
	
	if form.is_valid():
		form.save()
		
	response = {'form': form}
	if(request.method == 'POST'):
		return redirect('/lab-3')
	return render(request, "lab3_form.html", response)