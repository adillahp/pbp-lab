from django import forms
from lab_2.models import Note

class NoteForm(forms.ModelForm):
	class Meta:
		model = Note
		fields = "__all__"
		widgets = {
					'to_who': forms.TextInput(attrs={'class':'form-control', 'placeholder':'To', 'style':'margin-top:20px;'}),
					'from_who': forms.TextInput(attrs={'class':'form-control', 'placeholder':'From', 'style':'margin-top:20px'}),
					'title': forms.TextInput(attrs={'class':'form-control', 'placeholder':'Title', 'style':'margin-top:20px'}),
					'message': forms.Textarea(attrs={'class':'form-control', 'placeholder':'Message', 'style':'margin-top:20px'}),
				  }
