1. Apakah perbedaan antara JSON dan XML?
	JSON (JavaScript Object Notation):
		1. JSON merupakan format pertukaran data yang didasarkan pada bahasa pemrograman Javascript
		2. Mennggunakan struktur data map
		3. Lebih cepat dan ringan dibandingkan XML
		4. Tidak mendukung namespaces, comments, dan metadata
		5. Mendukung penggunaan array
		6. Tidak menggunakan tag pembuka dan penutup
		7. Hanya mendukung tipe data primitif seperti string, integer, boolean, dan object

	XML (Extensible markup language):
		1. Digunakan untuk membawa data bukan menampilkan data
		2. Menggunakan struktur data tree
		3. Lebih lambat dibandingkan JSON
		4. Mendukung namespaces, comments, dan metadata
		5. Tidak mendukung penggunaan array
		6. Menggunakan tag pembuka dan penutup
		7. Mendukung tipe data primitif (string, integer, boolean) maupun tipe data kompleks seperti charts, gambar, dan tipe data non-primitif lainnya


2. Apakah perbedaan antara HTML dan XML?
	HTML (Hyper Text Markup Language):
		1. Merupakan markup languange untuk menampilkan data
		2. tag pembuka dan penutup merupakan predefined tags
		3. Tidak case sensitive
		4. Static, karena digunakan untuk menampilkan data
		5. Tag penutup boleh tidak digunakan
		6. Jumlah tag yang bisa dipakai terbatas

	XML (Extensible markup language):
		1. Merupakan markup language yang digunakan untuk pertukaran data
		2. tag pembuka dan penutup didefinisikan oleh user
		3. Case sensitive
		4. Dynamic, karena digunakan untuk pertukaran data
		5. Tag penutup harus digunakan
		6. Jumlah tag tidak terbatas karena dibuat oleh user


Referensi:
https://hackr.io/blog/json-vs-xml
https://www.geeksforgeeks.org/difference-between-json-and-xml/
https://www.geeksforgeeks.org/html-vs-xml/
