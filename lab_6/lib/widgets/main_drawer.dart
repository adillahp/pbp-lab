import 'package:flutter/material.dart';
import '../screens/lokasi_utd_screen.dart';
import '../screens/faq_screen.dart';
import '../screens/login_screen.dart';

class MainDrawer extends StatelessWidget {
  Widget buildListTile(String title, IconData icon, Function tapHandler) {
    return ListTile(
      leading: Icon(
        icon,
        size: 26,
      ),
      title: Text(
        title,
        style: TextStyle(
          fontFamily: 'RobotoCondensed',
          fontSize: 24,
          fontWeight: FontWeight.bold,
        ),
      ),
      onTap: (){tapHandler();},
    );
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
          Container(
            height: 100,
            width: double.infinity,
            padding: EdgeInsets.all(20),
            alignment: Alignment.centerLeft,
            color: Color(0xff203b61),
          ),
          SizedBox(
            height: 20,
          ),
		  buildListTile('Home', Icons.home,(){
			  Navigator.of(context).pushReplacementNamed('/');
		  }),
          buildListTile('Lokasi UTD', Icons.local_hospital,(){
			  Navigator.of(context).pushReplacementNamed(LokasiUTDScreen.routeName);
		  }),
          buildListTile('FAQ', Icons.question_answer_outlined,(){
			  Navigator.of(context).pushReplacementNamed(FAQScreen.routeName);
		  }),
          buildListTile('Login', Icons.account_box, (){
			  Navigator.of(context).pushReplacementNamed(LoginScreen.routeName);
		  }),
        ],
      ),
    );
  }
}
