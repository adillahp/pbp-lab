import 'package:flutter/material.dart';

class UTDForm extends StatefulWidget {
  const UTDForm({Key? key}) : super(key: key);

  @override
  UTDFormState createState() {
    return UTDFormState();
  }
}

class UTDFormState extends State<UTDForm> {
	final _formKey = GlobalKey<FormState>();
	
	@override
  	Widget build(BuildContext context) {
   
    return Form(key: _formKey,
				child:Container(
					padding:EdgeInsets.all(20),
					child:Column(
					crossAxisAlignment: CrossAxisAlignment.start,  
					children: <Widget>[
						Container(
							margin: EdgeInsets.only(bottom: 25),
							child:TextFormField(
						  	decoration: new InputDecoration(
							  labelText: "Kota",
							  border: OutlineInputBorder(
							  borderRadius: new BorderRadius.circular(5.0)),
						    ),
						  ),
						),
						Container(
							margin: EdgeInsets.only(bottom: 25),
							child:TextFormField(
						  	decoration: new InputDecoration(
							  labelText: "Alamat",
							  border: OutlineInputBorder(
							  borderRadius: new BorderRadius.circular(5.0)),
						    ),
						  ),
						),
						Center(
						   child:RaisedButton(
							 color: Color(0xffaf0011), 
						     onPressed: (){},
						     child: Text('Search'),
							 textColor: Colors.white,
						   ),
						),
					],
				),
			),
		);

  }
}