import 'package:flutter/material.dart';
import '../screens/login_screen.dart';

class SignUpForm extends StatefulWidget {
  const SignUpForm({Key? key}) : super(key: key);

  @override
  SignUpFormState createState() {
    return SignUpFormState();
  }
}

class SignUpFormState extends State<SignUpForm> {
	final _formKey = GlobalKey<FormState>();
	
	@override
  	Widget build(BuildContext context) {
   
    return Form(
			child:Container(
				padding:EdgeInsets.only(top:35, left:20,right:20, bottom:20),
				child:Column(
					crossAxisAlignment: CrossAxisAlignment.start,  
					children: <Widget>[
						Container(
							margin: EdgeInsets.only(bottom: 25),
							child:TextFormField(
								decoration:  InputDecoration(
									labelText: "Username",
									border: OutlineInputBorder(
									borderRadius:  BorderRadius.circular(5.0)),
								),
							),
						),
						Container(
							margin: EdgeInsets.only(bottom: 25),
							child:TextFormField(
								decoration:  InputDecoration(
									labelText: "Email",
									border: OutlineInputBorder(
									borderRadius:  BorderRadius.circular(5.0)),
								),
							),
						),
						Container(
							margin: EdgeInsets.only(bottom: 25),
							child:TextFormField(
								decoration: InputDecoration(
									labelText: "Password",
									border: OutlineInputBorder(
										borderRadius: BorderRadius.circular(5.0)),
								),
							),
						),
						Container(
							margin: EdgeInsets.only(bottom: 25),
								child:TextFormField(
									decoration: InputDecoration(
										labelText: "Confirm Password",
										border: OutlineInputBorder(
											borderRadius: BorderRadius.circular(5.0)),
									),
								),
						),
						Center(
							child:RaisedButton(
								color: Color(0xffaf0011), 
								onPressed: (){},
								child: Text('Register'),
								textColor: Colors.white,
							),
						),
						Center(
							child:FlatButton(
								onPressed: () {
									Navigator.of(context).pushReplacementNamed(LoginScreen.routeName);
								},
								child: Text("Sudah memiliki akun?"),
							)
						),
					],
				),
			),
		);

  }
}