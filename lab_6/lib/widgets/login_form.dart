import 'package:flutter/material.dart';
import '../screens/signup_screen.dart';

class LoginForm extends StatefulWidget {
  const LoginForm({Key? key}) : super(key: key);

  @override
  LoginFormState createState() {
    return LoginFormState();
  }
}

class LoginFormState extends State<LoginForm> {
	final _formKey = GlobalKey<FormState>();
	
	@override
  	Widget build(BuildContext context) {
   
    return Form(key: _formKey,
		child:Container(
		padding:EdgeInsets.only(top:35, left:20,right:20, bottom:20),
		child:Column(
			    crossAxisAlignment: CrossAxisAlignment.start,  
				children: <Widget>[
					Container(
						margin: EdgeInsets.only(bottom: 25),
					  	child:TextFormField(
							decoration: InputDecoration(
							labelText: "Username",
							border: OutlineInputBorder(
								borderRadius: BorderRadius.circular(5.0)),
							),
						),
					),
					Container(
						margin: EdgeInsets.only(bottom: 25),
						child:TextFormField(
							decoration: InputDecoration(
								labelText: "Password",
								border: OutlineInputBorder(
								borderRadius: BorderRadius.circular(5.0)),
							),
						),
					),
					Center(
						child:RaisedButton(
							color: Color(0xffaf0011), 
							onPressed: (){},
							child: Text('Login'),
							textColor: Colors.white,
						),
					),
					Center(
						child:FlatButton(
							onPressed: () {
							Navigator.of(context).pushReplacementNamed(SignupScreen.routeName);
							},
							child: Text("Belum memiliki akun?"),
							)
					),
				],
			),
		),
	);

  }
}