import 'package:flutter/material.dart';
import './screens/homepage_screen.dart';
import './screens/lokasi_utd_screen.dart';
import './screens/faq_screen.dart';
import './screens/login_screen.dart';
import './screens/signup_screen.dart';

void main() {
  runApp(const KonvaSearchApp());
}

class KonvaSearchApp extends StatelessWidget {
	const KonvaSearchApp({Key? key}) : super(key: key);

	@override
	Widget build(BuildContext context) {
		return MaterialApp(
			title: 'KonvaSearch',
			theme: ThemeData(
				primarySwatch: Colors.blue,
			),
			home: MyHomePage(title:RichText(text:TextSpan(
						style: TextStyle(
							color:Colors.red,
							fontSize: 25,
						),
						children: <TextSpan>[
							TextSpan(text: 'Konva'),
							TextSpan(text: 'Search', style: new TextStyle(color: Color(0xffb71c1c))),
						],
					),
				),						  
			),
		);
	}
}

class MyHomePage extends StatefulWidget {
	const MyHomePage({Key? key, required this.title}) : super(key: key);
	
	final RichText title;

	@override
	State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
	@override
  Widget build(BuildContext context) {
    return MaterialApp(
	  initialRoute: '/', 
      routes: {
        '/': (ctx) => HomePageScreen(widget.title),
        LokasiUTDScreen.routeName: (ctx) => LokasiUTDScreen(widget.title),
        FAQScreen.routeName: (ctx) => FAQScreen(widget.title),
        LoginScreen.routeName: (ctx) => LoginScreen(widget.title),
        SignupScreen.routeName: (ctx) => SignupScreen(widget.title),
      },
      onUnknownRoute: (settings) {
        return MaterialPageRoute(
          builder: (ctx) => HomePageScreen(widget.title),
        );
      },
	);
  }
}
