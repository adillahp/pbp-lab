import 'package:flutter/material.dart';
import '../widgets/main_drawer.dart';
import '../widgets/utd_form.dart';

class LokasiUTDScreen extends StatefulWidget {
  final RichText title;

  LokasiUTDScreen(this.title);
  static const routeName = '/lokasi-donor';
  @override
  _LokasiUTDScreenState createState() => _LokasiUTDScreenState();
}

class _LokasiUTDScreenState extends State<LokasiUTDScreen> {
	final _formKey = GlobalKey<FormState>();
	Widget buildTableRow(String nama, String kota, String alamat, String jam_operasi, String kontak, Color boxColor, Color textColor) {
		return Table(  
                    defaultColumnWidth: FixedColumnWidth(70),  
                    border: TableBorder.all(  
                        style: BorderStyle.solid,  
                        width: 2),  
                    children: [  
                      TableRow(
						decoration: BoxDecoration(color: boxColor),
						children: [  
                        Column(children:[Text(nama, style: TextStyle(fontSize: 15.0,color:textColor))]),  
                        Column(children:[Text(kota, style: TextStyle(fontSize: 15.0, color:textColor))]),  
                        Column(children:[Text(alamat, style: TextStyle(fontSize: 15.0, color:textColor))]),
                        Column(children:[Text(jam_operasi, style: TextStyle(fontSize: 15.0, color:textColor))]),
                        Column(children:[Text(kontak, style: TextStyle(fontSize: 15.0, color:textColor))]),
                      ]),  
                    ],  
                  );
  }
	
	@override
	Widget build(BuildContext context) {
    return Scaffold(
		drawer: Container(
				width: MediaQuery.of(context).size.width * 0.5,
				child:MainDrawer(),
		),
		appBar: AppBar(
			title: widget.title,
			iconTheme: IconThemeData(color: Colors.red),
			backgroundColor: Color(0xfff8f9fa),
		),
		backgroundColor: Colors.white,
		body: SingleChildScrollView(
			child:Stack(children: <Widget>[
					Column(children: <Widget>[
					UTDForm(),
					buildTableRow('Nama','Kota', 'Alamat', 'Jam Operasi', 'Kontak', Color(0xff343a40), Colors.white),

					],
				),
			],
      ),
	),
	);
  }
}