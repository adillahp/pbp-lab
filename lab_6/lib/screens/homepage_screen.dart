import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import '../widgets/main_drawer.dart';
import '../screens/login_screen.dart';

class HomePageScreen extends StatefulWidget {
  final RichText title;

  HomePageScreen(this.title);

  @override
  _HomePageScreenState createState() => _HomePageScreenState();
}

class _HomePageScreenState extends State<HomePageScreen> {
	
	@override
	Widget build(BuildContext context) {
    return Scaffold(
		drawer: Container(
				width: MediaQuery.of(context).size.width * 0.5,
				child:MainDrawer(),
		),
		appBar: AppBar(
			title: widget.title,
			iconTheme: IconThemeData(color: Colors.red),
			backgroundColor: Color(0xfff8f9fa),
		),
		backgroundColor: Color(0xffdceaf9),
		body: SingleChildScrollView(child:Stack(
			children: <Widget>[
				Column(
					crossAxisAlignment: CrossAxisAlignment.stretch,
					mainAxisAlignment: MainAxisAlignment.start,
					children: <Widget>[
						Container(
							width: MediaQuery.of(context).size.width,
							height: MediaQuery.of(context).size.height*0.47,
							padding:EdgeInsets.all(20),
							color: Color(0xffd1deec),
							child: Column(
								crossAxisAlignment: CrossAxisAlignment.start,
								mainAxisAlignment: MainAxisAlignment.start,
								children: <Widget>[
									Text('Donasi Plasma Konvalesen',
										style: TextStyle(fontSize: 50),
									),
									Container(
										margin: EdgeInsets.only(top: 25),
										child:RaisedButton(  
											child: Text("Donasi", style: TextStyle(fontSize: 20)),
											color: Color(0xff203b61),  
											textColor: Colors.white,  
											padding: EdgeInsets.all(8),  
											onPressed: ()=>{Navigator.of(context).pushReplacementNamed(LoginScreen.routeName)}, 
										),
									),
								],
							),
							
						),
						CarouselSlider(
						   items: [
								Container(
								  child: Column(children:[
									  Text('Apa itu plasma konvalesen?',textAlign: TextAlign.center,style: TextStyle(fontSize: 35)),
									  Divider(color: Colors.black),
									  Text('Plasma adalah bagian dari darah yang mengandung antibodi. Plasma konvalensen adalah plasma darah yang diambil dari pasien yang telah dinyatakan sembuh dari Covid-19.',textAlign: TextAlign.center,style: TextStyle(fontSize: 18))
								  ]),
								  margin: EdgeInsets.all(6),
								),
								Container(
								  child: Column(children:[
									  Text('Bagaimana cara kerjanya?',textAlign: TextAlign.center,style: TextStyle(fontSize: 35)),
									  Divider(color: Colors.black),
									  Text('Plasma konvalesen dari donor diharapkan memiliki antibodi terhadap virus SARS-CoV-2 sehingga dapat menurunkan gejala, mencegah perkembangan virus, dan mempercepat proses penyembuhan pada penerima donor.',textAlign: TextAlign.center,style: TextStyle(fontSize: 18))
								  ]),
								  margin: EdgeInsets.all(6),
								),
							],

						  options: CarouselOptions(
							height: 280,
							enlargeCenterPage: true,
							autoPlay: true,
							autoPlayCurve: Curves.fastOutSlowIn,
							enableInfiniteScroll: true,
							autoPlayAnimationDuration: Duration(milliseconds: 800),
							viewportFraction: 1,
						  ),
          				),
						Container(margin:EdgeInsets.only(left:20, right:20, bottom:10),
								child:Text('Pendonor', style:TextStyle(fontSize: 22, color:Colors.red))),
						Container(
							margin: EdgeInsets.only(left:20, right:20, bottom:5),
							child:ClipRRect(
						  	borderRadius: BorderRadius.circular(8),
						  	child: Container(
							height: 15,
							child: LinearProgressIndicator(
								value: 0.4, 
								valueColor: AlwaysStoppedAnimation<Color>(Colors.red),
								backgroundColor: Color(0xffe9ecef),
							  ),
							),
						 ),
					   ),
					  Container(margin:EdgeInsets.only(left:20, right:20, bottom:10),
								child:Text('4 orang ingin mendonorkan plasmanya', style:TextStyle(fontSize: 20))),
					  Container(margin:EdgeInsets.only(top:10, left:20, right:20, bottom:10),
								child:Text('Pencari Donor', style:TextStyle(fontSize: 22, color:Colors.red))),
					  Container(
							margin: EdgeInsets.only(left:20, right:20, bottom:5),
							child:ClipRRect(
						  	borderRadius: BorderRadius.circular(8),
						  	child: Container(
							height: 15,
							child: LinearProgressIndicator(
								value: 0.4, 
								valueColor: AlwaysStoppedAnimation<Color>(Colors.red),
								backgroundColor: Color(0xffe9ecef),
							  ),
							),
						 ),
					   ),
					   Container(margin:EdgeInsets.only(left:20, right:20, bottom:10),
								child:Text('4 orang sedang mencari donor plasma', style:TextStyle(fontSize: 20))),
					],
				),
				
			],
		 ),
	   ),
    );
  }
}