import 'package:flutter/material.dart';
import '../widgets/main_drawer.dart';
import '../widgets/login_form.dart';

class LoginScreen extends StatefulWidget {
  final RichText title;

  LoginScreen(this.title);
  static const routeName = '/login';

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
	@override
	Widget build(BuildContext context) {
    return Scaffold(
		drawer: Container(
				width: MediaQuery.of(context).size.width * 0.5,
				child:MainDrawer(),
		),
		appBar: AppBar(
			title: widget.title,
			iconTheme: IconThemeData(color: Colors.red),
			backgroundColor: Color(0xfff8f9fa),
		),
		backgroundColor: Color(0xffdceaf9),
		body: SingleChildScrollView(child:Stack(
			children: <Widget>[
				Center(
					child:Container(
						padding:EdgeInsets.only(top: 20),
						child:RichText(
							text:TextSpan(
								style: TextStyle(
									color:Colors.red,
									fontSize: 40,
								),
								children: <TextSpan>[
									TextSpan(text: 'Konva'),
									TextSpan(text: 'Search', style: new TextStyle(color: Color(0xffb71c1c))),
								],
							),
						),
					),
				),
				Column(
					crossAxisAlignment: CrossAxisAlignment.start,
					mainAxisAlignment: MainAxisAlignment.start,
					children: <Widget>[
						Container(
							decoration: BoxDecoration(
							  color: Colors.white,
							  borderRadius: BorderRadius.circular(25),
							  border: Border.all(color: Color(0xffcc1728)),
						    ),
							margin:EdgeInsets.only(top:80, left:20, right:20),
							child: LoginForm(),
						),
				    ],
				),
			],
		),
		),
    );
  }
}