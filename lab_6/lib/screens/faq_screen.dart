import 'package:flutter/material.dart';
import '../widgets/main_drawer.dart';
import '../screens/login_screen.dart';

class FAQScreen extends StatefulWidget {
  final RichText title;

  FAQScreen(this.title);
  static const routeName = '/faq';

  @override
  _FAQScreenState createState() => _FAQScreenState();
}

class _FAQScreenState extends State<FAQScreen> {
	Widget buildExpansionTile(String pertanyaan, String jawaban){
  	return ExpansionTile(
              title: Text(
                pertanyaan,
                style: TextStyle(
                  fontSize: 18.0,
				  color: Colors.blue
                ),
              ),
              children: <Widget>[
                ListTile(
                  title: Text(
                    jawaban
                  ),
                )
              ],
            );
  }
	@override
	Widget build(BuildContext context) {
    return Scaffold(
		drawer: Container(
				width: MediaQuery.of(context).size.width * 0.5,
				child:MainDrawer(),
		),
		appBar: AppBar(
			title: widget.title,
			iconTheme: IconThemeData(color: Colors.red),
			backgroundColor: Color(0xfff8f9fa),
		),
		backgroundColor: Colors.white,
		body: SingleChildScrollView(child:Stack(
			children: <Widget>[
				Column(
					crossAxisAlignment: CrossAxisAlignment.start,
					mainAxisAlignment: MainAxisAlignment.start,
					children: <Widget>[
						Container(
							padding:EdgeInsets.all(20),
							child:RichText(text:TextSpan(
							style: TextStyle(
								color:Colors.red,
								fontSize: 25,
							),
							children: <TextSpan>[
								TextSpan(text: 'Frequently '),
								TextSpan(text: 'Asked ', style: new TextStyle(color: Color(0xffb71c1c))),
								TextSpan(text: 'Question', style: new TextStyle(color: Color(0xff050359))),
							],
							),),
						),
						buildExpansionTile(
							'Apa itu Plasma Konvalesen? ',
							'Plasma konvalesen adalah plasma darah dari pasien yang telah sembuh/penyintas Covid-19. Plasma darah pasien penyintas ini mengandung antibodi atau kekebalan tubuh terhadap Covid-19.'
						),
						buildExpansionTile(
							' Apa itu Terapi Plasma Konvalesen? ',
							'Terapi plasma konvalesen merupakan salah satu metode pengobatan yang kini digunakan untuk menangani pasien COVID-19, khususnya dengan gejala berat. Terapi plasma konvalesen adalah pemberian plasma darah donor atau sumbangan dari pasien yang telah sembuh dari COVID-19 (penyintas COVID-19) kepada pasien COVID-19. '
						),
						buildExpansionTile(
							'Apa tujuan Terapi Plasma Konvalesen? ',
							'''- Mempercepat penyembuhan dan pemulihan
- Meringankan gejala yang dialami, seperti sesak napas, nyeri dada, atau demam
- Mencegah komplikasi dan menurunkan tingkat keparahan penyakit
- Menurunkan risiko kematian '''
						),
						buildExpansionTile(
							'Apa saja kriteria pendonor Terapi Plasma Konvalesen? ',
							'Terapi plasma konvalesen merupakan salah satu metode pengobatan yang kini digunakan untuk menangani pasien COVID-19, khususnya dengan gejala berat. Terapi plasma konvalesen adalah pemberian plasma darah donor atau sumbangan dari pasien yang telah sembuh dari COVID-19 (penyintas COVID-19) kepada pasien COVID-19. '
						),
						buildExpansionTile(
							'Apa itu Plasma Konvalesen? ',
							'''Para penyintas COVID-19 yang ingin melakukan donor plasma darah harus memenuhi kriteria berikut:
1. Berusia 18–60 tahun
2. Memiliki riwayat positif COVID-19 dalam 3 bulan terakhir
3. Dalam kondisi sehat dan sudah dinyatakan sembuh dari COVID-19 minimal selama 14 hari
4. Diutamakan laki-laki, atau perempuan yang belum pernah hamil
5. Memiliki berat badan minimal 55 kg
6. Tidak memiliki riwayat tranfusi darah dalam 6 bulan terakhir
7. Dalam kondisi sehat dan tidak memiliki penyakit menular melalui darah, seperti hepatitis atau HIV/AIDS
8. Memiliki kadar antibodi virus Corona yang cukup
9. Memiliki golongan darah yang cocok dengan penerima '''
						),
						buildExpansionTile(
							'Siapa yang Memerlukan Terapi Plasma Konvalesen? ',
							'Sasaran terapi plasma konvalesen adalah pasien Covid-19. Tapi tidak semuanya bisa menerima terapi plasma. Syarat pasien boleh mendapat donor plasma berisi antibodi ini adalah berusia 18 tahun ke atas, tes usap dari nasofaring/orofaring dengan pemeriksaan NAAT (Nucleic Acid Amplification Tests) metode RT-PCR (Reverse Transcriptase-Polymerase Chain Reaction) menunjukkan hasil positif, dan memiliki gejala sedang, berat, hingga kritis dan dirawat di instalasi rawat inap khusus Covid-19 atau unit perawatan intensif di rumah sakit '
						),
						buildExpansionTile(
							'Apakah Terapi Plasma Konvalesen ini aman? ',
							'Berdasarkan data penelitian yang valid, TPK dinyatakan aman. Efek samping berupa alergi memiliki insidensi 1:5.000 '
						),
						Container(
							padding:EdgeInsets.all(20),
							child:RichText(text:TextSpan(
							style: TextStyle(
								color:Colors.red,
								fontSize: 25,
							),
							children: <TextSpan>[
								TextSpan(text: 'Any '),
								TextSpan(text: 'Other ', style: new TextStyle(color: Color(0xffb71c1c))),
								TextSpan(text: 'Question?', style: new TextStyle(color: Color(0xff050359))),
							],
							),),
						),
						Container(
							padding:EdgeInsets.only(left:20),
							margin: EdgeInsets.only(bottom:20),
							child:RaisedButton(  
							child: Text("Login", style: TextStyle(fontSize: 20),),
							color: Color(0xff203b61),  
							textColor: Colors.white,  
							onPressed: ()=>{Navigator.of(context).pushReplacementNamed(LoginScreen.routeName)},  
							),
						),
				    ],
				),
			],
		),
		),
    );
  }
}